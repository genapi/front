import axios, {AxiosInstance, AxiosRequestConfig, AxiosPromise} from 'axios';
import env from '../environments/environment';
import queryString from 'query-string';
import { Base64 } from 'js-base64';
import TokenRepository from '../repository/token.repository';

export default class BaseService {

    public clientId = 'default';

    public http: AxiosInstance;

    // private token: any;

    constructor() {
        this.http = axios.create({
            baseURL: env.baseURL
        });
    }

    public beforeRequest() {
        const token = this.getToken();
        if(token !== null) {
            this.http.defaults.headers['Authorization'] = 'Bearer ' + token.access_token;
        }
    }

    public setToken(token: any): void {
        TokenRepository.setToken(token);
        // this.token = token;
        this.http.defaults.headers['Authorization'] = 'Bearer ' + token.access_token;
    }

    public getToken() {
        const getedToken = TokenRepository.getToken();
        if(getedToken === null) return null
        return JSON.parse(getedToken);
    }

    public removeToken() {
        delete this.http.defaults.headers['Authorization'];
        TokenRepository.removeToken();
    }

    public getPublicKey(): AxiosPromise<any> {
        return this.http.get<string>('/back/oauth/token/public', {
            headers: {}
        });
    }

    public refreshToken() {
        delete this.http.defaults.headers['Authorization'];
        return new Promise((resolve, reject) => {
            this.http.post('/back/oauth/token', queryString.stringify({
                grant_type: 'refresh_token',
                refresh_token: this.getToken().refresh_token
            }),
            {
                baseURL: env.baseURL,
                headers: {
                    'Authorization': 'Basic ' + Base64.encode(`${this.clientId}:${this.clientId}`),
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then((res) => {
                this.setToken(res.data);
                this.resolve(resolve, res.data);
            }).catch(reject);
        });
    }



    public get<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
        this.beforeRequest();
        return new Promise<T>((resolve, reject) => {
            this.http.get<T>(url, config)
                .then((res) => this.resolve(resolve, res))
                .catch((res) => {
                    if(res.message === 'Request failed with status code 401' && this.getToken() !== null) {
                        this.refreshToken()
                            .then((res)=> {
                                this.http.get<T>(url, config)
                                    .then((res) => this.resolve(resolve, res))
                                    .catch((res) => this.resolve(reject, res))
                            })
                            .catch((res) => {
                                this.reject(reject, res);
                            })
                        return;
                    }
                    this.reject(reject, res);
                })
        })
    }

    public post<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
        this.beforeRequest();
        return new Promise<T>((resolve, reject) => {
            this.http.post<T>(url, data ? data : {}, config)
            .then((res) => this.resolve(resolve, res))
            .catch((res) => {
                if(res.message === 'Request failed with status code 401' && this.getToken() !== null) {
                    this.refreshToken()
                        .then((res)=> {
                            this.http.post<T>(url, data ? data : {}, config)
                                .then((res) => this.resolve(resolve, res))
                                .catch((res) => this.resolve(reject, res))
                        })
                        .catch((res) => {
                            this.reject(reject, res);
                        })
                    return;
                }
                this.reject(reject, res);
            })
        })
    }

    public put<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
        this.beforeRequest();
        return new Promise<T>((resolve, reject) => {
            this.http.put<T>(url, data ? data : {}, config)
                .then((res) => this.resolve(resolve, res))
                .catch((res) => {
                    if(res.message === 'Request failed with status code 401' && this.getToken() !== null) {
                        this.refreshToken()
                            .then((res)=> {
                                this.http.put<T>(url, data ? data : {}, config)
                                    .then((res) => this.resolve(resolve, res))
                                    .catch((res) => this.resolve(reject, res))
                            })
                            .catch((res) => {
                                this.reject(reject, res);
                            })
                        return;
                    }
                    this.reject(reject, res);
                })
        })
    }

    public delete<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
        this.beforeRequest();
        return new Promise<T>((resolve, reject) => {
            this.http.delete<T>(url, config)
                .then((res) => this.resolve(resolve, res))
                .catch((res) => {
                    if(res.message === 'Request failed with status code 401' && this.getToken() !== null) {
                        this.refreshToken()
                            .then((res)=> {
                                this.http.delete<T>(url, config)
                                    .then((res) => this.resolve(resolve, res))
                                    .catch((res) => this.resolve(reject, res))
                            })
                            .catch((res) => {
                                this.reject(reject, res);
                            })
                        return;
                    }
                    this.reject(reject, res);
                })
        })
    }    

    public resolve(callback: (res?: any) => void, res?: any) {
        if(res) return callback(res.data);
        return callback();
    }

    public reject(callback: (res?: any) => void, res?: any) {
        if(res) return callback(res);
        return callback();
    }
}