import BaseService from './base.service';
import ModelInterface from '../models/model.interface';
import RowInfoInterface from '@/models/row-info.interface';
import TableRequestInterface from '@/models/table-request.interface';
import TableResponseInterface from '@/models/table-response.interface';

export default class ModelsService extends BaseService {

    private url = '/back/api/models';

    public getRows(request: TableRequestInterface): Promise<TableResponseInterface> {
        return this.post<TableResponseInterface>(`${this.url}/get-rows`, request);
    }

    public getInfoRows(projectId: number): Promise<RowInfoInterface[]> {
        return this.get<RowInfoInterface[]>(`${this.url}/get-info-rows?projectId=${projectId}`);
    }

    public getAllInfoRows(): Promise<RowInfoInterface[]> {
        return this.get<RowInfoInterface[]>(`${this.url}/get-all-info-rows`);
    }

    public getById(id: number): Promise<ModelInterface> {
        return this.get<ModelInterface>(this.url + `/${id}`);
    }

    public create(model: ModelInterface): Promise<ModelInterface> {
        return this.post<ModelInterface>(this.url + '/create', model);
    }

    public update(model: ModelInterface): Promise<ModelInterface> {
        return this.put<ModelInterface>(this.url + '/update', model);
    }

    public remove(id: number): Promise<ModelInterface> {
        return this.delete<ModelInterface>(`${this.url}/delete/${id}`);
    }

}