import BaseService from './base.service';
import env from '../environments/environment';
import queryString from 'query-string';
import { Base64 } from 'js-base64';
import UserInterface from '../models/user.interface';

export default class AuthService extends BaseService {

    public login(login: string, password: string): Promise<UserInterface> {
        return new Promise((resolve, reject) => {
            this.http.post('/back/oauth/token', queryString.stringify({
                grant_type: 'password',
                username: login,
                password: password,
                client_id: this.clientId
            }),
            {
                baseURL: env.baseURL,
                headers: {
                    'Authorization': 'Basic ' + Base64.encode(`${this.clientId}:${this.clientId}`),
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then((res) => {
                this.setToken(res.data);
                this.resolve(resolve, res)
            }).catch((res) => {
                this.reject(reject, res)
            });
        })
    }

}