import BaseService from './base.service';
import UserInterface from '../models/user.interface';
import UserDetailsInterface from '../models/user-details.interface';
import ProjectUserInterface from '@/models/project-user.interface';
import ProjectUserDetailsInterface from '@/models/project-user-details.interface';
import TableResponseInterface from '@/models/table-response.interface';
import TableRequestInterface from '@/models/table-request.interface';

export default class UsersService extends BaseService {

    private url = '/back/api/users';
    
    public getCurrentUser(): Promise<UserInterface> {
        return this.get<UserInterface>(this.url + '/current');
    }

    public create(user: UserDetailsInterface): Promise<UserInterface> {
        return this.post<UserInterface>(this.url + '/create', user);
    }

    public createForProject(user: ProjectUserDetailsInterface): Promise<ProjectUserInterface> {
        return this.post<ProjectUserInterface>(this.url + '/create-for-project-client', user)
    }

    public getProjectUsersByClientId(clientId: string): Promise<ProjectUserInterface[]> {
        return this.get(`${this.url}/get-project-client-users/${clientId}`);
    }

    public getProjectUsersRows(request: TableRequestInterface): Promise<TableResponseInterface> {
        return this.post<TableResponseInterface>(`${this.url}/get-project-client-users-rows`, request);
    }

    public deleteProjectUser(id: number): Promise<ProjectUserInterface> {
        return this.delete(`${this.url}/delete-project-user/${id}`)
    }

}