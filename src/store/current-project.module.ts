import ProjectInterface from '../models/project.interface';
import { ACTION_LOGOUT } from './current-user.module';


export const ACTION_SELECT_CURRENT_PROJECT = 'ACTION_SELECT_PROJECT';
export const ACTION_RESET_CURRENT_PROJECT = 'ACTION_RESET_CURRENT_PROJECT';

export const SET_CURRENT_PROJECT = 'ACTION_SELECT_PROJECT';


export const GET_CURRENT_PROJECT = 'ACTION_SELECT_PROJECT';


const module = {
    state: {
        currentProject: null
    },
    getters: {
        [GET_CURRENT_PROJECT]: (state: any) => {
            return state.currentProject !== null ? {...state.currentProject} : null;
        }
    },
    mutations: {
        [SET_CURRENT_PROJECT]: (state: any, project: ProjectInterface) => {
            return state.currentProject = project;
        }
    },
    actions: {
        [ACTION_SELECT_CURRENT_PROJECT]: (action: any, project: ProjectInterface) => {
            action.commit(SET_CURRENT_PROJECT, project);
        },
        [ACTION_RESET_CURRENT_PROJECT]: (action: any) => {
            action.commit(SET_CURRENT_PROJECT, null);
        },
        [ACTION_LOGOUT]: (action: any) => {
            action.commit(SET_CURRENT_PROJECT, null);
        }
    }
}

export default module;