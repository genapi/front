import ErrorBuilderInterface from './error-builder.interface';

export default interface ProjectUserDetailsInterface {
    id: number | null;
    login: string;
    password: string;
    clientId: string;
    errors: ErrorBuilderInterface | null;
}