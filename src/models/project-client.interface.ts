import ErrorBuilderInterface from './error-builder.interface';

export default interface ProjectClientInterface {
    clientId: string;
    projectId: number;
    projectName: string;
    projectOwner: number;
    errors: ErrorBuilderInterface | null;
}