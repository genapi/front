import ErrorBuilderInterface from './error-builder.interface';

export default interface ListInterface {
    id: number | null;
    name: string;
    projectId: number;
    type: string;
    list: any[];
    errors: ErrorBuilderInterface | null;
}