import ModelFieldInterface from './model-field.interface';
import ErrorBuilderInterface from './error-builder.interface';

export default interface ModelInterface {
    id: number | null;
    name: string;
    projectId: number;
    model: ModelFieldInterface[];
    bodyCount: number;
    listCount: number;
    errors: ErrorBuilderInterface | null;
}