import { RouteConfig } from 'vue-router';

export default interface NavItemInterface extends RouteConfig {
    icon?: string;
    showInMenu?: boolean;
    meta: {
        roles: string[],
        projectIsDependecy?: boolean
    },
    children?: NavItemInterface[];
}

export function mapToRouteConfig(navItems: NavItemInterface[]): RouteConfig[] {
    navItems.map((item: NavItemInterface) => {
        const resultItem: RouteConfig = {
            path: item.path,
            name: item.name,
            component: item.component
        }

        if(item.components) resultItem.components = item.components;
        if(item.redirect) resultItem.redirect = item.redirect;
        if(item.alias) resultItem.alias = item.alias;
        if(item.children) resultItem.children = mapToRouteConfig(item.children);
        if(item.beforeEnter) resultItem.beforeEnter = item.beforeEnter;
        if(item.props) resultItem.props = item.props;
        if(item.caseSensitive) resultItem.caseSensitive = item.caseSensitive;
        if(item.pathToRegexpOptions) resultItem.pathToRegexpOptions = item.pathToRegexpOptions;
        if(item.meta) resultItem.meta = item.meta;

        return resultItem;
    });
    return navItems
}