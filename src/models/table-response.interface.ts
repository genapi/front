export default interface TableResponseInterface {
    data: any[];
    count: number;
}