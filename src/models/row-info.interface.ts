export default interface RowInfoInterface {
    id: number;
    name: string;
}