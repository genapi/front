const ResponseTypeConstants = {
    JSON: 'JSON',
    XML: 'XML'
}

export default ResponseTypeConstants;