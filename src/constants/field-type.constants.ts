const FieldTypeConstants = {
    INTAGER: 'INTAGER',
    INTAGER_LIST: 'INTAGER[]',
    FLOAT: 'FLOAT',
    FLOAT_LIST: 'FLOAT[]',
    STRING: 'STRING',
    STRING_LIST: 'STRING[]',
    BOOLEAN: 'BOOLEAN',
    BOOLEAN_LIST: 'BOOLEAN[]',
    IMAGE: 'IMAGE',
    IMAGE_LIST: 'IMAGE[]',
    MODEL: 'MODEL',
    MODEL_LIST: 'MODEL[]'
}

export default FieldTypeConstants;