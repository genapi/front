import NavItemInterface from './models/nav-item.interface';
import RoutePage from './pages/route.page.vue';
import HomePage from './pages/home.page.vue';
import LoginPage from './pages/login.page.vue';
import RegistrationPage from './pages/registration.page.vue';
import ModelsPage from './pages/models/models.page.vue';
import EditModelPage from './pages/models/edit-model.page.vue';
import BodyListPage from './pages/body/body-list.page.vue';
import EditBodyPage from './pages/body/edit-body.page.vue';
import ProjectsPage from './pages/projects/projects.page.vue';
import ProjectRoutesPage from './pages/project-routes.page.vue';
import ListsPage from './pages/lists/lists.page.vue';
import EditListPage from './pages/lists/edit-list.page.vue';
import ProjectClientsPage from './pages/projects/project-clients.page.vue';
import ProjectUsersPage from './pages/projects/project-users.page.vue';

const nav: NavItemInterface[] = [
	{
		path: '/',
		name: 'Home',
		component: HomePage,
		icon: 'move_to_inbox',
		showInMenu: true,
		meta: {
			roles: ['USER']
		}
	},
	{
		path: '/projects',
		name: 'Projects',
		component: RoutePage,
		icon: 'move_to_inbox',
		showInMenu: true,
		redirect: '/projects/list',
		meta: {
			roles: ['USER']
		},
		children: [{
			path: 'list',
			name: 'List projects',
			component: ProjectsPage,
			// icon: 'move_to_inbox',
			showInMenu: true,
			meta: {
				roles: ['USER']
			}
		},
		{
			path: 'client',
			name: 'Project Clients',
			component: ProjectClientsPage,
			// icon: 'move_to_inbox',
			showInMenu: true,
			meta: {
				roles: ['USER']
			}
		},
		{
			path: 'users',
			name: 'Project Users',
			component: ProjectUsersPage,
			// icon: 'move_to_inbox',
			showInMenu: true,
			meta: {
				roles: ['USER']
			}
		}]
	},
	{
		path: '/models',
		name: 'Models',
		component: RoutePage,
		icon: 'move_to_inbox',
		showInMenu: true,
		meta: {
			roles: ['USER']
		},
		children: [{
			path: '',
			name: 'Models',
			component: ModelsPage,
			showInMenu: false,
			meta: {
				roles: ['USER']
			}
		},
		{
			path: 'edit/:id',
			name: 'Edit Model',
			component: EditModelPage,
			showInMenu: false,
			meta: {
				roles: ['USER']
			}
		},
		{
			path: 'new',
			name: 'New Model',
			component: EditModelPage,
			showInMenu: false,
			meta: {
				roles: ['USER']
			}
		}]
	},
	{
		path: '/body-list',
		name: 'Body List',
		component: RoutePage,
		icon: 'move_to_inbox',
		showInMenu: true,
		meta: {
			roles: ['USER']
		},
		children: [{
			path: '',
			name: 'Body List',
			component: BodyListPage,
			showInMenu: false,
			meta: {
				roles: ['USER']
			}
		},
		{
			path: 'edit/:id',
			name: 'Edit Body',
			component: EditBodyPage,
			showInMenu: false,
			meta: {
				roles: ['USER']
			}
		},
		{
			path: 'new',
			name: 'New Body',
			component: EditBodyPage,
			showInMenu: false,
			meta: {
				roles: ['USER']
			}
		}]
	},
	{
		path: '/routes',
		name: 'Routes',
		component: ProjectRoutesPage,
		icon: 'move_to_inbox',
		showInMenu: true,
		meta: {
			roles: ['USER']
		}
	},
	{
		path: '/lists',
		name: 'Lists',
		component: RoutePage,
		icon: 'move_to_inbox',
		showInMenu: true,
		meta: {
			roles: ['USER']
		},
		children: [{
			path: '',
			name: 'Lists',
			component: ListsPage,
			showInMenu: false,
			meta: {
				roles: ['USER']
			}
		},
		{
			path: 'edit/:id',
			name: 'Edit List',
			component: EditListPage,
			showInMenu: false,
			meta: {
				roles: ['USER']
			}
		},
		{
			path: 'new',
			name: 'New List',
			component: EditListPage,
			showInMenu: false,
			meta: {
				roles: ['USER']
			}
		}]
	},
	{
		path: '/login',
		name: 'Login',
		component: LoginPage,
		icon: 'move_to_inbox',
		showInMenu: false,
		meta: {
			roles: ['ANONYM']
		}
	},
	{
		path: '/registration',
		name: 'Registration',
		component: RegistrationPage,
		icon: 'move_to_inbox',
		showInMenu: false,
		meta: {
			roles: ['ANONYM']
		}
	},
	{
		path: '/404',
		name: '404',
		component: LoginPage,
		icon: 'move_to_inbox',
		showInMenu: false,
		meta: {
			roles: ['ANONYM', 'USER']
		}
	}
];

export default nav;